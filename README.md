# Cofidis Public API
CAUTION! This document is not a final one. Some of data, and structure during conciliation.
## API
- Based on HTTP with JSON responses

## Authentication
- Based on JSON Web Token
- Token issuer must be Cofidis
- Token subject must refers to the client uuid
- The token must be signed with a shared secret
- Token must contains an expiration time

An example token:
```javascript
{
  "alg": "HS256",
  "typ": "JWT"
}
{
  "iss": "Cofidis",
  "sub": "39178740-aa3f-11e8-a137-529269fb1459",
  "exp": "1735689600"
}
```
## Headers

This header MUST be included in all requests that require authentication/authorization.
The auth scheme is using a JSON Web Token Bearer Token as the header value.
See [JWT](http://jwt.io/introduction/) for an introduction to JWT.

Example:

```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJJQk0gQnVkYXBlc3QgTGFiIiwic3ViIjoiMTIzNDUiLCJleHAiOiIxNzM1Njg5NjAwIn0.rGrYuJQ3kgfae1pDxoK6BgA3WT1ydEGsCICGNT31F6A
```

## Basic inforamtion

| Name            | Value          |
| -------------   | -------------  |
| API url         | https://api.cofidis.hu |
| API sandbox url | https://sandbox-api.cofidis.hu       |
| version |v1.01|


## Api
### /loan
It means user could create, and reade state of loan data witch equals the old webshop ticket.

### Getting API user related loan data list
User can earn all of his loan data

> GET | https://api.cofidis.hu/v1/loan

#### Response example
```javascript
[
  {
    "webshop_id":15252,
    "identification_type": 1,
    "order_id": "2018-07-29 18:00:00",
    "name":"Jhon Doe",
    "mothername":"Mother of Jhon Doe",
    "email":"info@test.com",
    "phone":"+36304266178",
    "price":"199000",
    "birthdate":"1980-11-10",
    "product":[
      "teszt termek"
    ],
    "loanpercent": 19.99,
    "loanmaturity":24,
    "downpayment": 0,
    "yearpercent": 19.99,
    "selectedinstamountbyprd": 10020,
    "selectedinstamountbyprdwithins": 10020,
    "insurancetype": 0,
    "khr": 1,
    "status" : "refused", //during conciliation
    "files": [],
  },
  {
    "webshop_id":15254,
    "identification_type": 2,
    "order_id": "2018-07-29 18:00:00",
    "name":"Jhon Doe",
    "mothername":"Mother of Jhon Doe",
    "email":"info@test.com",
    "phone":"+36304266178",
    "price":"199000",
    "birthdate":"1980-11-10",
    "product":[
      "teszt termek"
    ],
    "loanpercent": 19.99,
    "loanmaturity":24,
    "downpayment": 0,
    "yearpercent": 19.99,
    "selectedinstamountbyprd": 10020,
    "selectedinstamountbyprdwithins": 10020,
    "insurancetype": 0,
    "khr": 1,
    "status" : "new",//during conciliation
    "files": [
      "contract1.pdf"
    ],
  }
]
```

#### Possible negative responses
- 401 Unauthorized: The provided Access Token is missing, revoked, expired or malformed the request
- 404 Item(s) not found

### Create new loan data

> POST | https://api.cofidis.hu/v1/loan

#### Parameters

Media type: **application/json**

**Properties**

| name | Type |Instruction | Example |
| ----:| ----------- | ----------- | - |
| order_time | datetime | DateTime of the of the order| 2018-08-11 15:00:00 |
| name | string | Name of the user | Jhon Doe |
| mother_name | string | Mother name of the user | Mother of Jhon Doe |
| email | string | Email of the user | info@email.com |
| phone | string | Phone number of the user | +36701234567 |
| birth_date | date | Birth date of the user | 1982-05-15 |
| loan_price | float |  | 19.99 |
| loan_percent | float |  | 19.99 |
| loan_maturity | float | number of the months | 24 |
| downpayment | float |  | 0 |
| products | array [string] | Array of proudct strings | |
| yearpercent | float | | 19.99 |
| selectedinstamountbyprd | float | | 10020 |
| selectedinstamountbyprdwithins | float | | 10020 |
| insurancetype | tinyint |  | 0
| khr | tinyint | 0:(nem nyilatkozott) 1:(nyilatkozott) | 1 |


#### Request example
```javascript
{
  "webshop_id":15254,
  "identification_type": 2,
  "order_id": "2018-07-29 18:00:00",
  "name":"Jhon Doe",
  "mothername":"Mother of Jhon Doe",
  "email":"info@test.com",
  "phone":"+36304266178",
  "price":"199000",
  "birthdate":"1980-11-10",
  "product":[
    "teszt termek"
  ],
  "loanpercent": 19.99,
  "loanmaturity":24,
  "downpayment": 0,
  "yearpercent": 19.99,
  "selectedinstamountbyprd": 10020,
  "selectedinstamountbyprdwithins": 10020,
  "insurancetype": 0,
  "khr": 1
}
```

#### Response example

```javascript
{
  "id": 12345,
  "status": "created",
}
```

#### Possible negative responses
- 401 Unauthorized: The provided Access Token is missing, revoked, expired or malformed
- 429 To many request
- 400 Bad Request: There are mising field (example_filed)

### /loan/{id}
Working with specific loan data

### Request an loan item

> GET | https://api.cofidis.hu/v1/loan/{id}

#### Query parameter
- **id** - The id of loan entity

#### Response example
```javascript
{
  "webshop_id":15254,
  "identification_type": 2,
  "order_id": "2018-07-29 18:00:00",
  "name":"Jhon Doe",
  "mothername":"Mother of Jhon Doe",
  "email":"info@test.com",
  "phone":"+36304266178",
  "price":"199000",
  "birthdate":"1980-11-10",
  "product":[
    "teszt termek"
  ],
  "loanpercent": 19.99,
  "loanmaturity":24,
  "downpayment": 0,
  "yearpercent": 19.99,
  "selectedinstamountbyprd": 10020,
  "selectedinstamountbyprdwithins": 10020,
  "insurancetype": 0,
  "khr": 1,
  "status" : "accepted" //during conciliation
}
```

### Update Loan Data

> PUT | https://api.cofidis.hu/v1/loan/{id}

#### Mandotary Parameter
- **id** - id of the edited loan data

#### Request example
```javascript
{
  "id": 123, // mandotary
  "webshop_id":15254,
  "identification_type": 2,
  "order_id": "2018-07-29 18:00:00",
  "name":"Jhon Doe",
  "mothername":"Mother of Jhon Doe",
  "email":"info@test.com",
  "phone":"+36304266178",
  "price":"199000",
  "birthdate":"1980-11-10",
  "product":[
    "teszt termek"
  ],
  "loanpercent": 19.99,
  "loanmaturity":24,
  "downpayment": 0,
  "yearpercent": 19.99,
  "selectedinstamountbyprd": 10020,
  "selectedinstamountbyprdwithins": 10020,
  "insurancetype": 0,
  "khr": 1
}
```

### Set ready for processing

> PUT | https://api.cofidis.hu/v1/loan/{id}/ready

#### Success response
```javascript
status-code:200
{}
```

### Error response
```javascript
status-code:400
{
  "errors": [
    {"some data are missing"}
  ]
}
```
